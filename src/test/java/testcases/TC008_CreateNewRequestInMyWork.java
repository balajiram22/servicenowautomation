package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class TC008_CreateNewRequestInMyWork {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\KRISHNA KUMAR\\git\\TestcaseAutomation\\servicenowautomation\\driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://dev69221.service-now.com/");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		driver.switchTo().frame("gsft_main");
		driver.findElementById("user_name").sendKeys("admin");
		driver.findElementById("user_password").sendKeys("India@123");
		driver.findElementById("sysverb_login").click();
		
		//driver.switchTo().frame("gsft_main");
		driver.findElementById("filter").sendKeys("service desk");
		driver.findElementByXPath("(//div[text()='My Work'])[1]").click();
		driver.switchTo().frame("gsft_main");
		Thread.sleep(8000);
		driver.findElementByXPath("//table[@id='task_table']//tr[1]//td[3]//a").click();
	    //driver.switchTo().frame("gsft_main");
		driver.findElementById("sc_request.description").clear();
		driver.findElementById("sc_request.short_description").clear();
		Thread.sleep(3000);
		driver.findElementByXPath("//div/button[@id='sysverb_update']").click();
		Thread.sleep(5000);
				
		String errmessage = driver.findElementByXPath("//span[@class='outputmsg_text']").getText();
		if (errmessage.equals("The following mandatory fields are not filled in: Short description, Description"))
				System.out.println("Error message is popped up");
		else {
			System.out.println("no error message");
		}
		driver.close();
      
		
	}
	}


