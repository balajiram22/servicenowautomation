package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TC003_OrderForReplacementWithoutOriginalPhoneInfo {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\KRISHNA KUMAR\\servicenowautomation\\driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://dev69221.service-now.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.switchTo().frame("gsft_main");
		driver.findElementById("user_name").sendKeys("admin");
		driver.findElementById("user_password").sendKeys("India@123");
		driver.findElementById("sysverb_login").click();
		
		
		driver.findElementByXPath("//div[text()='Service Catalog']").click();
		Thread.sleep(8000);
		driver.switchTo().frame("gsft_main");
		Thread.sleep(8000);
		driver.findElementByXPath("//h2[text()[normalize-space()='Mobiles']]").click();
		Thread.sleep(8000);
		driver.findElementByXPath("//strong[text()='Apple iPhone 6s']").click();
		driver.findElementByXPath("(//label[@class='radio-label'])[2]").click();
		Thread.sleep(3000);
		driver.findElementById("oi_order_now_button").click();
		
		Thread.sleep(3000);
		
		String errorMessageTheFollowing = driver.findElementByXPath("(//div[@class='outputmsg_div']//div)[3]").getText();
		if(errorMessageTheFollowing.contains("What was the original phone number?")){

			System.out.println("Error Message displayed to Enter the original phone info");
		}
		else {
			System.out.println("no Error Message dispalyed");
		}
	}
		
		
		
			

	}


