package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TC004_UpdateExistingOrder {
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://dev77567.service-now.com/");
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	driver.switchTo().frame("gsft_main");
	driver.findElement(By.name("user_name")).sendKeys("admin");
	driver.findElement(By.name("user_password")).sendKeys("India@123");
	driver.findElement(By.name("not_important")).click(); 	
	driver.findElement(By.xpath("(//input[@type='search'])[3]")).sendKeys("Request",Keys.ENTER);
	//Thread.sleep(2000);
	driver.findElement(By.xpath("//span[text()='My Requests']")).click();
	
	driver.switchTo().frame("gsft_main");
	driver.findElement(By.linkText("REQ0010023")).click();
	
	WebElement approval = driver.findElement(By.xpath("(//select[@class='form-control  '])[1]"));
	Select select = new Select(approval);
	select.selectByVisibleText("Requested");
	
	driver.findElement(By.xpath("(//textarea[@class='form-control'])[1]")).sendKeys("Apple Service");
	driver.findElement(By.xpath("(//input[@class='form-control'])[1]")).sendKeys("iphone service");
	
	driver.findElement(By.xpath("//div[@class='form_action_button_container']/button[1]")).click();

}
}
