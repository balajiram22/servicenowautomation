package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class TC010_CreateNewRequestInMyWork_N {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://dev69221.service-now.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().frame("gsft_main");
		driver.findElement(By.xpath("//label[text()='User name']/following::input")).sendKeys("admin");
		driver.findElementByXPath("//input[@type='password']").sendKeys("India@123");
		driver.findElementById("sysverb_login").click();
		driver.findElementByXPath("//input[@name='filter']").sendKeys("My Work",Keys.ENTER);
		Thread.sleep(3000);
		driver.findElementByXPath("//div[text()='My Work']").click();
		driver.switchTo().frame("gsft_main");
		driver.findElementByXPath("//table[@id='task_table']//tr[1]//td[3]//a").click();
		//driver.findElementByXPath("//input[@id='sys_display.chat_queue_entry.cmdb_ci']").sendKeys("MySQL FLX",Keys.ENTER);
		//driver.findElementByXPath("//input[@id='sys_display.chat_queue_entry.parent']").sendKeys("CHAT0010001",Keys.ENTER);
		driver.findElementByXPath("//textarea[@id='sc_request.description']").clear();
		driver.findElementByXPath("//input[@id='sc_request.short_description']").clear();
		driver.findElementByXPath("//div[@class='form_action_button_container']/child::button[text()='Update']").click();
		String expText = driver.findElementByXPath("//span[@class='outputmsg_text']").getText();
		String actualText = "The following mandatory fields are not filled in: Short description, Work notes";
		if(expText.contains(actualText))
		{
			System.out.println("The Expected text is present -" + expText);
		}
	}

}
