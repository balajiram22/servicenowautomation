package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testleaf.serviceNow.pages.ServiceCatalogPage;

public class TC001_OrderAProduct {
	 public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\git\\servicenowautomation\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://dev85820.service-now.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
		driver.switchTo().frame(0);
		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("user_password")).sendKeys("India@1234");
		driver.findElement(By.id("sysverb_login")).click();
		String title=  driver.getTitle();
		System.out.println("Title of the Home page is: " + title);

		driver.findElement(By.xpath("(//div[@class='sn-widget-list-title'])[4]")).click();
		driver.switchTo().frame(0);
		driver.findElement(By.xpath("//h2[text()[normalize-space()='Mobiles']]")).click();

		driver.findElement(By.xpath("//strong[text()='Apple iPhone 6s']")).click();

		WebElement allowance = driver.findElement(By.xpath("(//select[@class='form-control cat_item_option '])[1]")); 
		
		Select allowance1=new Select(allowance);
		allowance1.selectByIndex(2); 
		Thread.sleep(3000);
		WebElement color = driver.findElement(By.xpath("(//select[@class='form-control cat_item_option '])[2]")); 
		Select color1=new Select(color);
		color1.selectByIndex(3); 
		WebElement storage = driver.findElement(By.xpath("(//select[@class='form-control cat_item_option '])[3]"));
		Select storage1=new Select(storage);
		storage1.selectByIndex(0);
		driver.findElement(By.id("oi_order_now_button")).click();
		String text = driver.findElement(By.xpath("//span[contains(text(),'Thank you, your request has been submitted')]")).getText();
		System.out.println(text);

		if (text.contains("Thank you, your request has been submitted")) {
			System.out.println("Order placed");
			String requestnumber = driver.findElement(By.xpath("/html[1]/body[1]/div[3]/dl[1]/dd[2]/a[1]/b[1]")).getText();
			System.out.println(requestnumber);
		} else {
			System.out.println("Order not placed");
		}
	}}

	
