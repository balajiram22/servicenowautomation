package testcases;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC005_CancelOrder {

	public static void main(String[] args) throws Exception {
		//System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				ChromeDriver driver = new ChromeDriver();
				
				driver.get("https://dev77567.service-now.com");
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.switchTo().frame("gsft_main");
				driver.findElement(By.xpath("//label[text()='User name']/following::input")).sendKeys("admin");
				driver.findElementByXPath("//input[@type='password']").sendKeys("India@123");
				driver.findElementById("sysverb_login").click();
				driver.findElementByXPath("//input[@name='filter']").sendKeys("Request",Keys.ENTER);
				//driver.findElementByXPath("//span[text()='My Requests']").click();
				driver.findElementByXPath("//div[text()='Requests']").click();
				driver.switchTo().frame("gsft_main");
				driver.findElementByXPath("//a[contains(text(),'REQ')]").click();
				
				//Set<String> winhandles = driver.getWindowHandles();
				//winhandles.size();
				/*driver.findElementByXPath("//input[@id='sc_request.number']").sendKeys(Keys.TAB);
				driver.findElementByXPath("//div[@class='input-group ref-container ']/input").sendKeys(Keys.TAB);
				driver.findElementByXPath("//input[@id='sys_display.sc_request.location']").sendKeys(Keys.TAB);
				driver.findElementByXPath("//input[@id='sc_request.due_date']").sendKeys(Keys.TAB);
				driver.findElementByXPath("//input[@id='sys_display.sc_request.opened_by']").sendKeys(Keys.TAB);*/
				
								Thread.sleep(6000);
				//WebDriverWait wait = new WebDriverWait(driver,60);
				//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("gsft_main"));
				//driver.switchTo().frame("gsft_main");
				WebElement src = driver.findElementByXPath("//select[@name='sc_request.approval']");
				
				Select dd = new Select(src);
				dd.selectByValue("rejected");
				
		WebElement src2 = driver.findElementByXPath("//select[@name='sc_request.request_state']");
		Select dd2 = new Select(src2);
		dd2.selectByValue("closed_cancelled");
		
		driver.findElementByXPath("//textarea[@id='sc_request.description']").sendKeys("Test",Keys.TAB);
		driver.findElementByXPath("//input[@id='sc_request.short_description']").sendKeys("Test",Keys.TAB);
		driver.findElementByXPath("//div[@class='form_action_button_container']/button[2]").click();
		
				

	}

}
